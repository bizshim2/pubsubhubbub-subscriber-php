# README #

### Google PubsubHubbub Subscrierコールバック用ソースコード ###

* xmlやlogファイルを出力したいディレクトリをしてい。(要書き込み権限）
```
$save_path = '';
```

* HTTPアクセス可能なサーバーに配備

### 手順1:Subscribe登録 ###
https://pubsubhubbub.appspot.com/subscribe

にアクセスし、「Subscribe/Unsubscribe」箇所に下記を入力

「Callback URL」　に本ソースをデプロイしたURLを指定
「Topic URL」　取得したいフィードのURLを指定
「Mode」登録したい場合はSubscribe / 解除したい場合はUnSubscribe

設定後「Do It!」ボタンを押下（次画面では真っ白な画面が表示されるが問題ありません）

### 手順2:Subscribe登録確認 ###
https://pubsubhubbub.appspot.com/subscribe

にアクセスし、「Subscriber Diagnostics」箇所に下記を入力

「Callback URL」　に本ソースをデプロイしたURLを指定
「Topic URL」　取得したいフィードのURLを指定

設定後「Do It!」ボタンを押下
State: verify
が表示されていれば登録されています。

### 確認方法 ###

Publish後、指定したファイル出力パスにフィードXMLファイルが作成されていれば成功です。