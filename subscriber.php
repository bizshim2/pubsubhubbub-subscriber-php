<?php
    $save_path = '';// ファイル出力先パスをしてします。※ここのフォルダは権限666推奨
    main();

    function main(){
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == 'GET') {
            doGet();
        } else if ($method == 'POST') {
            doPost();
        } else {
            putLog("Unexpected Method:{$method}");
				}
    }

    function doGet(){
        putLog("doGet() start");
        $verify_token = "";
        // subscribe (or unsubscribe) a feed from the HUB
        $hubmode = $_REQUEST['hub_mode'];
        $hubchallenge = $_REQUEST['hub_challenge'];
        putLog("hubmode = {$hubmode}");
        putLog("hub_verify_token =  " . $_REQUEST['hub_verify_token']);
        if ($hubmode == 'subscribe' || $hubmode == 'unsubscribe') {
            if ($_REQUEST['hub_verify_token'] != $verify_token) {//verify_tokenのチェック
                putLog("doGet() hub_verify_token unmatch");
                header('HTTP/1.1 404 "Unknown Request"', null, 404);
                exit("Unknown Request");
            }
            // response a challenge code to the HUB
            header('HTTP/1.1 200 "OK"', null, 200);
            header('Content-Type: text/plain');
            echo $hubchallenge;
        } else {
            header('HTTP/1.1 404 "Not Found"', null, 404);
            putLog("doGet() hubmode unmatch");
            exit("Unknown Request");
        }
        putLog("doGet() end");
    }

    function doPost(){
        putLog("doPost() start");
        $string = file_get_contents("php://input");
				file_put_contents($save_path . date('YmdHms') . "_atom" . ".xml", $string);
        $feed = simplexml_load_string($string);
        putLog(var_export($feed,true));
        putLog("doPost() end");
    }

    function putLog($buf){
        $date = date('Y/m/d H:m:s', time());
        $buf = $date." ". $buf;
				file_put_contents("{$save_path}log.txt", $buf . PHP_EOL, FILE_APPEND);
    }
?>
